/****************************************************

########## Project 1 Basis ##########

Graphics Processing - PG (if680)
Center of Informatics - CIn
Federal University of Pernambuco - UFPE

@authors
{
	Caio Lins (csnrl at cin.ufpe.br),
	Geovane Pereira (geeosp at cin.ufpe.br),
	Vinicius Emanuel (vems at cin.ufpe.br)
}

Reference for OpenGL commands: https://www.opengl.org/sdk/docs/man2/xhtml/

*****************************************************/

#include "Main.h"

// ##### Params START #####

// Extrinsic matrix translation params (camera translation)
// Note: only set for the z axis
double tc = -1.5;

// Extrinsic matrix
GLfloat extrinsic[16] =
{
	1, 0, 0, 0,
	0, 1, 0, 0,
	0, 0, -1, 0,
	-0.5, 0, tc, 1
};

// Object rotation param for all axis
double ro = 0;

// Object translation param for the x axis
double to = 0;

// ##### Params END #####

// Screen params
GLfloat wWidth = 1280;
GLfloat wHeight = 720.0;

// Constants for object translation and rotation and camera translation
const double translateConstant = 0.01;
const double rotateConst = 1.5;
const double translateCameraConst = 0.005;

bool buffer[250];

void initialize() // Initialize opengl params
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
	glClearDepth(1.0f);		
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);	
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void myreshape(GLsizei w, GLsizei h) // Called at startup and when you move the window
{
	glMatrixMode(GL_PROJECTION);
	double g_Width = wWidth;
	double g_Height = wHeight;
	glViewport(0, 0, g_Width, g_Height);	
	glLoadIdentity();
	gluPerspective(45, g_Width / g_Height, 0.1f, 3000.0f);
}

void drawGrid() // Draws a grid...
{
	glPushMatrix();

		glTranslatef(-5, -.1, -5);
	
		glColor3f(.3, .3, .3);

		glBegin(GL_LINES);
			for (int i = 0; i <= 10; i++)
			{
				if (i == 0) { glColor3f(.6, .3, .3); }
				else { glColor3f(.25, .25, .25); };
				glVertex3f(i, 0, 0);
				glVertex3f(i, 0, 10);
				if (i == 0) { glColor3f(.3, .3, .6); }
				else { glColor3f(.25, .25, .25); };
				glVertex3f(0, 0, i);
				glVertex3f(10, 0, i);
			};
		glEnd();

	glPopMatrix();
}

void mydisplay()
{
	cout << "Don't remove me, I am running." << endl;

	glMatrixMode(GL_MODELVIEW);

	glClearColor(0, 0, 0, 0);

	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	// Update camera translation on z axis (altered by keyboard input)
	extrinsic[14] = tc;
	
	glMatrixMode(GL_MODELVIEW);

	glLoadMatrixf(extrinsic);

	glRotatef(ro * 0.2, 0, 1, 0); // Comment this line to stop the world from spinning (less dizzy)

	drawGrid();

	glColor3f(0.2, 0.0, 0.2);

	// Test without glPushMatrix() and glPopMatrix() to see what happens
	glPushMatrix();

		glTranslatef(to, 0, 0);
		glRotatef(ro, 1, 1, 1);
		glutWireSphere(0.3, 100, 100);
	
	glPopMatrix();

	glColor3f(0.2, 0.2, 0.0);

	glPushMatrix();

		glTranslatef(to + 0.8, 0, 0);
		glRotatef(ro, 1, 1, 1);
		glutWireTeapot(0.3);

	glPopMatrix();

	glColor3f(0.0, 0.2, 0.2);

	glPushMatrix();

		glTranslatef(to - 0.6, 0, 0);
		glRotatef(ro, 1, 1, 1);
		glutWireCube(0.3);

	glPopMatrix();

	glFlush();

	glutPostRedisplay();
}

void handleKeyboardPressed(unsigned char key, int x, int y)
{
	buffer[(int)key] = true;
}

void handleKeyboardUp(unsigned char key, int x, int y)
{
	buffer[(int)key] = false;
}

void idleFunction() // Processes keyboard inputs
{
	// #### Resume ####

	// Move object: 1, 2
	// Rotate object: r
	// Move camera: w, s
	// Exit: ESC

	// #### Object commands ####

	if(buffer['1']==true) // Translate object on x negatively
		translate(-translateConstant);
	if(buffer['2']==true) // Translate object on x positively
		translate(translateConstant);
	if(buffer['r']==true)
		rotate(rotateConst);
	
	// #### Camera commands ####
	
	if(buffer['w']==true) // Move camera frontward
		cameraTranslate(translateCameraConst);
	if(buffer['s']==true) // Move camera backward
		cameraTranslate(-translateCameraConst);

	// #### Other commands ####

	if(buffer[27] == true) // ESC
		exit(0);
}

void translate(double t)
{
	to += t; // Translate object on x axis
}

void rotate(double r)
{
	ro += r; // Rotate object in all axis
}

void cameraTranslate(double t)
{
	tc += t; // Translate camera on z axis
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(0,0);
	glutCreateWindow("OpenGL");
	glutDisplayFunc(mydisplay);
	glutReshapeFunc(myreshape);
	glutKeyboardUpFunc(handleKeyboardUp);
	glutKeyboardFunc(handleKeyboardPressed);
	glutIdleFunc(idleFunction);
	initialize();
	glutMainLoop();
	return 0;
}